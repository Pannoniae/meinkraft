from mk.controller import GameController
from mk.images import image_process
from mk.io.load import GameLoader
from mk.io.save import GameSaver

VERSION = "0.5.6"


def main():
    image_process()
    ctrl = GameController(width=800, height=600, caption='My own cute Pyglet v%s' % VERSION,
                          resizable=True, vsync=True)
    # ctrl.logevents()
    # Hide the mouse cursor and prevent the mouse from leaving the window.
    ctrl.set_exclusive_mouse(True)
    ctrl.renderer.setup()
    ctrl.info("App started.")
    GameLoader(ctrl).load_game()
    ctrl.info("last game state loaded")
    ctrl.run()
    ctrl.info("App stopped.")
    GameSaver(ctrl).save_game()
    ctrl.info("game state saved")


if __name__ == '__main__':
    main()
