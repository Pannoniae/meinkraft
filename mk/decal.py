from pyglet.gl import GL_QUADS

from mk.data.blocks import DECAL


class Decal(object):
    def __init__(self, master, pos):
        from mk.controller import GameController
        assert isinstance(master, GameController)
        self.master = master
        self.position = pos
        self.vertexlist = None

    def draw(self):
        decal_inst = DECAL()
        vertex_data = decal_inst.get_vertices(*self.position)
        texture_data = list(decal_inst.get_texture())
        self.vertexlist = self.master.model.batch.add(24, GL_QUADS, self.master.model.group,
                                                      ('v3f/static', vertex_data),
                                                      ('t2f/static', texture_data))

    def delete(self):
        assert self.vertexlist, "must be drawn first!"
        self.vertexlist.delete()
